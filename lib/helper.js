'use strict';

const exec = require('child_process').execSync;
const fs = require('fs');
const chalk = require('chalk');

const logger = {
    out(log) {
        process.stdout.write(log);
    },
    warn(log) {
        process.stdout.write(chalk.yellow(log));
    },
    error(log) {
        process.stdout.write(chalk.red(log));
    },
    success(log) {
        process.stdout.write(chalk.green(log));
    },
    info(log) {
        process.stdout.write(chalk.blue(log));
    }
};
exports.logger = logger;

exports.execSync = (script, cwd) => {
    exec(script, cwd, (err, stdout, stderr) => {
        if (err) {
            logger.error(stderr);
            process.stdin.pause();
        }
        logger.out(stdout);
    });
};

exports.deleteFolderRecursive = (path) => {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach((file, index) => {
            const curPath = `${path}/${file}`;
            if (fs.lstatSync(curPath).isDirectory()) {
                // recurse
                exports.deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

module.exports = exports;
