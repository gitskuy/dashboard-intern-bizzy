#!/usr/bin/env node

const program = require('commander');
const co = require('co');
const prompt = require('co-prompt');
const serviceComnd = require('./service');
const installComnd = require('./install');
const { logger } = require('./helper');

program
    .command('install')
    .description('install')
    .action(() => {
        co(function* () {
            const os = yield prompt('Your Operating System \n [1] MacOS \n [2] Linux \n OS number: ');
            return os;
        }).then((os) => {
            switch (os) {
            case '1': installComnd.macos(); break;
            case '2': installComnd.linux.ubuntu(); break;
            default: logger.error(`OS not found for number ${os}\n`);
            }
        }).then(() => {
            serviceComnd.createConfigDir();
            process.stdin.pause();
        });
    });
program
    .command('service')
    .arguments('<actionType>')
    .description('Microservice')
    .action((actionType) => {
        co(function* () {
            switch (actionType) {
            // eslint-disable-next-line no-case-declarations
            // using scoped in 'case' to scoped variable for current case only
            case 'add': {
                const serviceName = yield prompt('Service Name: ');
                const branch = yield prompt('Branch / tag: ');
                serviceComnd.add(serviceName, branch);
                break;
            }
            // eslint-disable-next-line no-case-declarations
            case 'remove': {
                const name = yield prompt('Service Name: ');
                serviceComnd.remove(name);
                break;
            }
            case 'list': {
                serviceComnd.list();
                break;
            }
            case 'update': {
                serviceComnd.update();
                break;
            }
            case 'checkout-all': {
                const branch = yield prompt('Branch / tag: ');
                serviceComnd.checkoutAllTo(branch);
                break;
            }
            case 'checkout': {
                const serviceName = yield prompt('Service Name: ');
                const branch = yield prompt('Branch / tag: ');
                serviceComnd.checkoutTo(serviceName, branch);
                break;
            }
            case 'install': {
                const serviceName = yield prompt('Service Name: ');
                serviceComnd.installService(serviceName);
                break;
            }
            case 'install-all': {
                serviceComnd.installAllService();
                break;
            }
            default:
                // code block
            }
            process.stdin.pause();
        });
    });

program.parse(process.argv);
