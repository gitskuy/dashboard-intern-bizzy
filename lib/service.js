'use strict';

// TODO: if the current directory is wrong, abort current operationes ?

const path = require('path');
const helper = require('./helper');
const fs = require('fs');

const cwd = process.cwd();
const { logger } = helper;

const GIT_REMOTE_DEF = 'git@bitbucket.org:bizzyindonesia/';
const BRANCH_DEF = 'release';
const SERVICE_DIR_DEF = 'modules';
const CONF_DIR_DEF = 'config';
const CONF_FILE_DEF = 'modules.json';
const CONF_DIR_PATH = path.join(cwd, CONF_DIR_DEF);
const CONF_FILE_PATH = path.join(cwd, CONF_DIR_DEF, CONF_FILE_DEF);

exports.CONF_DIR = CONF_DIR_DEF;
exports.SERVICE_DIR = SERVICE_DIR_DEF;

// create configuration directory: config & modules dir
function createConfigDir() {
    const dirs = [CONF_DIR_DEF, SERVICE_DIR_DEF];
    dirs.forEach((dir) => {
        const pathDir = path.join(cwd, dir);
        if (!fs.existsSync(pathDir)) {
            process.stdout.write(`creating ${dir} directory\n`);
            fs.mkdirSync(pathDir);
        }
    });
}
exports.createConfigDir = createConfigDir;

// isConfDirExists check conf directory existance
const isConfDirExists = () => fs.existsSync(CONF_DIR_PATH);

// checkConfFile, check if CONF_FILE is exists
// return Promise with resolve() & reject(error)
const checkConfFile = () => new Promise((resolve, reject) => {
    fs.stat(CONF_FILE_PATH, (err, stats) => {
        if (err) resolve(false, err);
        else if (stats && !stats.isFile()) reject(new Error(`${CONF_FILE_DEF} is not a file`));
        else resolve(true, null);
    });
});

// TODO: check is it check if service is really in modules ?
// getService return array of service object
const getServices = () => new Promise((resolve, reject) => {
    fs.readFile(CONF_FILE_PATH, (err, data) => {
        if (err) {
            resolve([]);
        }

        try {
            // check if the file has contents and are json
            const confs = data.toString().length > 0
                ? JSON.parse(data.toString())
                : [];

            resolve(confs);
        } catch (error) { // eslint-disable-line no-unused-vars
            // no need to log error it just to handle try
            reject(error);
        }
    });
});

// findService find a service from given CONF_FILE
// return Promise with resolve(exist: boolean). reject(error)
const findService = name => new Promise(async (resolve, reject) => {
    try {
        if (!isConfDirExists()) throw new Error(`${CONF_DIR_DEF} directory not exists\n`);
        if (!await checkConfFile()) throw new Error(`${CONF_FILE_DEF} not found\n`);

        const isFound = (await getServices()).filter(sv => sv.name === name).length > 0;
        resolve(isFound);
    } catch (error) {
        reject(error);
    }
});

// write services to CONF_FILE,
// if services is empty, it will make CONF_FILE empty
const writeConfs = services => new Promise((resolve, reject) => {
    try {
        const prettyConfs = services.length > 0
            ? JSON.stringify(services, undefined, 2)
            : '';

        fs.writeFile(CONF_FILE_PATH, prettyConfs, 'utf8', (writeErr) => {
            if (writeErr) reject(Error(writeErr));
            else {
                resolve();
            }
        });
    } catch (error) {
        reject(error);
    }
});

// addConfig add a new service to CONF_FILE
async function addConfig({ serviceName, serviceBranch }) {
    try {
        // create conf folder if not exist
        // const pathDir = path.join(cwd, CONF_DIR);
        if (!isConfDirExists()) {
            fs.mkdirSync(CONF_DIR_PATH);
            logger.out(`Creating ${CONF_DIR_DEF} directory\n`);
        }

        if (!await checkConfFile()) {
            logger.out(`${CONF_FILE_DEF} not found. Create one\n`);
            helper.execSync('touch config/modules.json');
        } else {
            logger.out(`found ${CONF_FILE_DEF}\n`);
        }

        await writeConfs([...(await getServices()), { name: serviceName, branch: serviceBranch }]);
        logger.success(`success adding ${serviceName}\n`);
    } catch (error) {
        logger.error(`${error}\n`);
    }
}

// add a service by branch (or tag) or use DEFAULT_BRANCH
exports.add = async (serviceName, serviceBranch) => {
    try {
        if (!serviceName.trim()) throw new Error('service cannot be empty. aborted\n');

        logger.out(`adding service: ${serviceName}\n`);
        try {
            if (await findService(serviceName)) throw new Error('service already added. aborted\n');
        } catch (findError) {
            logger.out(`${findError}`);
        }

        let usedBranch = serviceBranch;

        if (!serviceBranch.trim()) {
            usedBranch = BRANCH_DEF;
            logger.warn(`using default branch: ${BRANCH_DEF}\n`);
        }

        const gitRepo = `${GIT_REMOTE_DEF}${serviceName}.git`;
        const serviceDir = path.join(cwd, SERVICE_DIR_DEF, serviceName);

        try {
            helper.execSync(`git clone ${gitRepo} ${serviceDir}`);
            helper.execSync(`git checkout  ${usedBranch}`, { cwd: serviceDir });

            addConfig({ serviceName, serviceBranch: usedBranch });
        // eslint-disable-next-line no-case-declarations
        } catch (error) { // eslint-disable-line no-unused-vars
            // no need to log error it just to handle exception
            //
            // delete service dir
            try {
                logger.error(`Can't add ${serviceName}.\nDeleting ${serviceDir}\n`);
                helper.deleteFolderRecursive(serviceDir);
            } catch (delErr) {
                throw delErr;
            }
        }
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

// rmServiceConf, remove service conf from CONF_FILE
// return Promise with resolve() reject(error)
const rmServiceConf = ({ name }) => new Promise(async (resolve, reject) => {
    try {
        if (!await checkConfFile()) throw new Error(`${CONF_FILE_DEF} not found, aborted\n`);
        const updatedConf = (await getServices()).filter(conf => conf.name !== name);
        await writeConfs(updatedConf);
        resolve();
    } catch (error) {
        reject(error);
    }
});

// remove a service
exports.remove = async (name) => {
    try {
        if (!await findService(name)) throw new Error('service not found\n');

        const serviceDir = path.join(cwd, SERVICE_DIR_DEF, name);
        helper.deleteFolderRecursive(serviceDir);

        await rmServiceConf({ name });
        logger.warn(`remove ${name} from ${CONF_FILE_DEF}\n`);
        logger.success(`delete success ${serviceDir}\n`);
    } catch (error) {
        logger.error('Something wrong. Aborted\n');
        logger.error(`${error}\n`);
    }
};

// list all installed service
exports.list = async () => {
    try {
        if (!isConfDirExists()) throw new Error(`${CONF_DIR_DEF} directory not exists\n`);
        if (!await checkConfFile()) throw new Error(`${CONF_FILE_DEF} not found\n`);

        const services = await getServices();
        if (services.length <= 0) throw new Error('no service installed\n');
        services.forEach((conf, idx) => {
            logger.out(`${idx + 1}. [${conf.branch}] ${conf.name}\n`);
        });
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

// update all installed services to match upstream
exports.update = async () => {
    try {
        const services = await getServices();
        // loop through service
        services.forEach((sv) => {
            const serviceDir = path.join(cwd, SERVICE_DIR_DEF, sv.name);
            // do update for every service
            helper.execSync(
                "find . -maxdepth 1 -type d -exec sh -c '(cd {} && git pull)' ';'",
                { cwd: serviceDir }
            );
        });
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

const checkout = (service, branch) => {
    const clonedSv = { ...service };
    const serviceDir = path.join(cwd, SERVICE_DIR_DEF, clonedSv.name);

    // try checkout to new branch in upstream,
    // or try checkout to existing branch
    try {
        helper.execSync(
            `git checkout -b ${branch} origin/${branch}`,
            { cwd: serviceDir }
        );

        // update current conf
        clonedSv.branch = branch;
        return { service: clonedSv, error: null };
    } catch (error) { // eslint-disable-line no-unused-vars
        // no need to stdout `error`

        try {
            helper.execSync(
                `git checkout ${branch}`,
                { cwd: serviceDir }
            );

            // update current conf
            clonedSv.branch = branch;
            return { service: clonedSv, error: null };
        } catch (coErr) {
            logger.error(`${coErr}\n`);

            // no change to current conf
            return { service: clonedSv, error: coErr };
        }
    }
};

// checkout a service to a given branch.
exports.checkoutTo = async (serviceName, branch) => {
    try {
        if (!await findService(serviceName)) throw new Error('service not found\n');

        const services = await getServices();
        const clonedSvs = services.map((sv) => {
            if (sv.name !== serviceName) return sv;

            const { service, error } = checkout(sv, branch);
            if (error) throw new Error(`cannot checkout ${serviceName} to branch ${branch}\n`);
            return service;
        });

        await writeConfs(clonedSvs);
        logger.success(`success checkout ${serviceName} to ${branch}\n`);
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

// checkout all services to an <branch>.
// if there is error when checkout-ing a branch,
// it will used previous branch and continue
exports.checkoutAllTo = async (branch) => {
    try {
        if (!branch || !branch.trim()) throw new Error('please input valid branch\n');
        const services = await getServices();
        if (services.length <= 0) throw new Error('no service installed\n');

        // update only for service that success to checkout
        // if all failed when checkout, newServices will be empty array
        // then it will throw Error and not outputting success message
        const newServices = services.reduce((acc, sv) => {
            const { service, error } = checkout(sv, branch);
            if (error) acc.push(sv);
            else acc.push(service);

            return acc;
        }, []);

        if (newServices.length <= 0) throw new Error('failed to checkout all services\n');
        await writeConfs(newServices);
        logger.success(`success checkout services to ${branch}\n`);
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

const installModule = serviceName => new Promise(async (resolve, reject) => {
    try {
        const installScript = "find bizzy-* -maxdepth 1 -type d -exec sh -c '(cd {} && npm install)' ';'";
        const svModuleDir = path.join(cwd, SERVICE_DIR_DEF, serviceName, SERVICE_DIR_DEF);
        helper.execSync(installScript, { cwd: svModuleDir });
        resolve();
    } catch (error) {
        reject(error);
    }
});

const installService = async (svName) => {
    try {
        if (!await findService(svName)) throw new Error(`${svName} not found\n`);
        logger.info(`found service ${svName}. \nInstalling modules...\n`);
        await installModule(svName);
        logger.success('installation success\n\n');
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

exports.installService = installService;

exports.installAllService = async () => {
    try {
        (await getServices()).forEach(async (sv) => {
            await installService(sv.name);
        });

        logger.success('installations finished\n\n');
    } catch (error) {
        logger.error(`${error}\n`);
    }
};

module.exports = exports;
