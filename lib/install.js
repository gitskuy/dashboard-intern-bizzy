'use strict';

const helper = require('./helper');
const fs = require('fs');
const path = require('path');

const { logger } = helper;

const CONSUL_VERSION = '1.4.0';
const PROCESSOR_ARCH = 'amd';
const OS_ARCH = '64';
const CONSUL_FILE = `consul_${CONSUL_VERSION}_linux_${PROCESSOR_ARCH}${OS_ARCH}.zip`;
const CONSUL_URL = `https://releases.hashicorp.com/consul/${CONSUL_VERSION}/${CONSUL_FILE}`;
const CONSUL_HOMEBREW = 'https://raw.githubusercontent.com/Homebrew/install/master/install';
const INSTALL_DIR = '/usr/local/bin/';

exports.macos = () => {
    try {
        helper.execSync(`/usr/bin/ruby -e "$(curl -fsSL ${CONSUL_HOMEBREW})" && brew install consul`);
        helper.execSync('brew install consul');
    } catch (error) {
        logger.error(error);
    }
};

const isConsulExist = () => {
    // check if path is exists
    const filePath = path.join(INSTALL_DIR, 'consul');
    if (!fs.existsSync(filePath)) {
        return false;
    }

    // check if it's a file
    const stats = fs.statSync(filePath);
    return stats && stats.isFile();
};

const ubuntu = () => {
    try {
        helper.execSync('sudo apt-get update');
        helper.execSync('sudo apt-get install unzip tmux nginx');

        if (isConsulExist()) {
            logger.warn('consul already installed\n');
            return;
        }

        logger.out('installing consul\n');

        helper.execSync(`sudo wget ${CONSUL_URL}`, { cwd: INSTALL_DIR });
        helper.execSync(`sudo unzip ${CONSUL_FILE}`, { cwd: INSTALL_DIR });
        helper.execSync(`sudo rm -rf ${CONSUL_FILE}`, { cwd: INSTALL_DIR });
    } catch (error) {
        logger.error(error);
    }
};

exports.linux = {
    ubuntu
};

module.exports = exports;
