# Developer
## Install
1. go to `bizzy-node-cli` folder
2. type in terminal
```terminal
npm i -g
which bz
readlink <copy then paste `which bz` output>
npm link
```

## TODO
- [ ] add unit test

## Features
- [x] clone service repo into `modules dir`
- [x] add cloned service to `config file`
- [x] remove cloned service from `config file`
- [x] remove cloned service from `modules dir`
- [x] update a service to match upstream
- [x] update all services to match upstream
- [x] install a service modules 
- [x] install all services modules 

## Bug
- [x] FIXME: when run `bizzy service add <service>` and get 
``
error: pathspec 'release' did not match any file(s) known to git.
``
it still `clone` the repo branch master, but not add to the `CONF_FILE` 
  - solutions:  
    - delete service if error happend

- [ ] When checkout all and all service failed to checkout, still output success message